<?php

    include_once "conexion.php";
    class Renaes
    {
        private $codigo;
        private $ess;
        private $distrito;
        private $provincia;

        function __construct($codigo,$ess,$distrito,$provincia)
        {
            $this->codigo=$codigo;
            $this->ess=$ess;
            $this->distrito=$distrito;
            $this->provincia=$provincia;
        }

        public static function constructorvacio()
        {
            return new self('','','','');
        }

        public function mostrarRenaes()
		{
			$db= new Conexion();
			$sql="SELECT codigo,ess,distrito,provincia FROM renaes";
			$resultado = $db->query($sql);
			return $resultado;
        }
        
        public function mostrarReanesId($codigo)
        {
            $db=new Conexion();
            $sql="SELECT codigo,ess,distrito,provincia  FROM renaes where codigo='$codigo'";
            $resultado= $db->query($sql);
            return $resultado;
        }

    }

?>