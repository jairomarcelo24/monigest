<?php

    class Conexion extends mysqli
    {
        private $servidor='localhost';
        private $basededatos='utesgestantes';
        private $usuario='root';
        private $password='';

        public function __construct()
        {
            parent:: __construct($this->servidor,$this->usuario,$this->password,$this->basededatos);
            $this->set_charset('utf-8');
            $this->connect_errno ? die('error en la conexion' . mysqli_connect_errno()): $m = "conectado";
        }
    }

?>