<?php

    include_once "conexion.php";

    class Control
    {
        private $codigo;
        private $cie1;
        private $cie2;
        private $cie3;
        private $cie4;
        private $paciente;
        private $comentarios;
        private $cpn;
        private $eg;
        private $estado;
        private $fechaInicio;
        private $horaInicio;
        private $fechaIngreso;
        private $horaIngreso;
        private $fechaEgreso;
        private $horaEgreso;
        private $g;
        private $grupoRiesgo;
        private $p1;
        private $p2;
        private $p3;
        private $p4;
        private $periodo;
        private $red;
        private $renaesOrigen;
        private $renaesDestino;
        private $responsable;
        private $serie;
        private $usuario;

        function __construct($cie1,$cie2,$cie3,$cie4,$paciente,$comentarios,$cpn,$eg,$estado,$fechaInicio,$horaInicio,$fechaIngreso,$horaIngreso,$fechaEgreso,$horaEgreso,$g,$grupoRiesgo,$p1,$p2,$p3,$p4,$periodo,$red,$renaesOrigen,$renaesDestino,$responsable,$serie,$usuario)
        {
            $this->cie1=$cie1;
            $this->cie2=$cie2;
            $this->cie3=$cie3;
            $this->cie4=$cie4;
            $this->paciente=$paciente;
            $this->comentarios=$comentarios;
            $this->cpn=$cpn;
            $this->eg=$eg;
            $this->estado=$estado;
            $this->fechaInicio=$fechaInicio;
            $this->horaInicio=$horaInicio;
            $this->fechaIngreso=$fechaIngreso;
            $this->horaIngreso=$horaIngreso;
            $this->fechaEgreso=$fechaEgreso;
            $this->horaEgreso=$horaEgreso;
            $this->g=$g;
            $this->grupoRiesgo=$grupoRiesgo;
            $this->p1=$p1;
            $this->p2=$p2;
            $this->p3=$p3;
            $this->p4=$p4;
            $this->periodo=$periodo;
            $this->red=$red;
            $this->renaesOrigen=$renaesOrigen;
            $this->renaesDestino=$renaesDestino;
            $this->responsable=$responsable;
            $this->serie=$serie;
            $this->usuario=$usuario;
        }

        public static function constructorvacio()
        {
            return new self('','','','','','','','','','','','','','','','','','','','','','','','','','','','');
        }


        public function insertarControl()
        {
            $db=new Conexion();
            $sql="INSERT INTO control (cie1, cie2,cie3,cie4,paciente,comentarios,cpn,eg,estado,fechaEgreso,horaEgreso,fechaIngreso,horaIngreso,fechaInicio,horaInicio,g,grupoRiesgo,p1,p2,p3,p4,periodo,red,renaesDestino,renaesOrigen,responsable,serie, usuario) VALUES ('$this->cie1', '$this->cie2', '$this->cie3', '$this->cie4', '$this->paciente', '$this->comentarios', '$this->cpn', '$this->eg', '$this->estado', '$this->fechaEgreso','$this->horaEgreso', '$this->fechaIngreso','$this->horaIngreso', '$this->fechaInicio','$this->horaInicio', '$this->g', '$this->grupoRiesgo', '$this->p1', '$this->p2', '$this->p3', '$this->p4', '$this->periodo', '$this->red', '$this->renaesDestino', '$this->renaesOrigen', '$this->responsable', '$this->serie', '$this->usuario')";
            $db->query($sql) ? $mm ="registro correcto" : $mm = "no se puede registrar";
            echo $mm;
        }

        public function actualizarControl($codigo)
        {
            $db = new Conexion();
            $sql = "UPDATE control SET cie1 = '$this->cie1',cie2 = '$this->cie2',cie3 = '$this->cie3',cie4 = '$this->cie4',paciente = '$this->paciente',comentarios = '$this->comentarios',cpn = '$this->cpn',eg = '$this->eg',estado = '$this->estado',fechaEgreso = '$this->fechaEgreso',horaEgreso = '$this->horaEgreso',fechaIngreso = '$this->fechaIngreso',horaIngreso = '$this->horaIngreso',fechaInicio = '$this->fechaInicio',horaInicio = '$this->horaInicio',g = '$this->g',grupoRiesgo = '$this->grupoRiesgo',p1 = '$this->p1',p2 = '$this->p2',p3 = '$this->p3',p4 = '$this->p4',periodo = '$this->periodo',red = '$this->red',renaesDestino = '$this->renaesDestino',renaesOrigen = '$this->renaesOrigen',responsable = '$this->responsable',serie = '$this->serie',usuario = '$this->usuario' WHERE codigo ='$codigo'";
            $db->query($sql) ? $mm = "registro actualizado " : $mm = "no se pudo actualizar";
            echo $mm;
        }

        public function buscarControlPaciente($dni)
        {
            $db= new Conexion();
            $sql="SELECT codigo FROM control WHERE paciente='$dni'";
            $resultado= $db->query($sql);
            return $resultado;
        }

        public function buscarControl($codigo)
        {
            $db = new Conexion();
            $sql="SELECT co.paciente as dniPaciente,co.fechaIngreso as fechaIngreso,co.horaIngreso as horaDeIngreso,co.serie as codigoSerie,co.red as codigoRed,
            co.renaesOrigen as codigoOrigen,reo.ess as essOrigen,reo.distrito as distritoOrigen,reo.provincia as provinciaOrigen, co.renaesDestino as codigoDestino,
            rede.ess as essDestino,rede.distrito as distritoDestino,rede.provincia as provinciaDestino,ci1.codigo as codigoCi1,ci1.descripcion as descripcionC1,
            ci2.codigo as codigoCi2,ci2.descripcion as descripcionC2,ci3.codigo as codigoCi3,ci3.descripcion as descripcionC3,ci4.codigo as codigoCi4,ci4.descripcion as descripcionC4,gru.codigo as codigogrupo,gru.descripcion as grupoRiesgo,
            co.fechaInicio as fechaInicio, co.horaInicio as horaInicio,co.fechaIngreso as fechaIngreso,co.responsable as responsable,
            co.g,co.p1,co.p2,co.p3,co.p4,co.eg,co.cpn,co.periodo,co.estado,co.comentarios,co.fechaEgreso as fechaEgreso,co.horaEgreso as horaDeEgreso
            from control co
            inner join renaes reo on co.renaesOrigen=reo.codigo
            inner join renaes rede on co.renaesDestino=rede.codigo
            inner join cie ci1 on co.cie1=ci1.codigo
            inner join cie ci2 on co.cie2=ci2.codigo
            inner join cie ci3 on co.cie3=ci3.codigo
            inner join cie ci4 on co.cie4=ci4.codigo
            inner join gruporiesgo gru on co.grupoRiesgo=gru.codigo
            where co.codigo='$codigo'";
            $resultado = $db->query($sql);
            return $resultado;
        }

        /*public function buscarControlActualizar($codigo)
        {
            $db = new Conexion();
            $sql="SELECT cie1, cie2, cie3, cie4, comentarios, cpn, eg, estado , fechaEgreso, horaEgreso, fechaIngreso, horaIngreso, fechaInicio, g, grupoRiesgo, p1, p2, p3, p4, periodo, red, renaesDestino, renaesOrigen, responsable, serie, usuario
            from control where codigo='$codigo'";
            $resultado = $db->query($sql);
            return $resultado;
        }*/

        // NECESITAN ALIAS PARA NO CONFUNDIR LAS COLUMNAS 
        public function mostrarReporte()
        {
            $db= new Conexion();
            $sql="SELECT co.codigo as codigoControl, CONCAT(p.apellidoPaterno,' ',p.apellidoMaterno,' ',p.nombres) as ppaciente,co.fechaIngreso as fechaIngreso,co.horaIngreso as horaDeIngreso,co.serie as codigoSerie,co.red as codigoRed,
            co.renaesOrigen as codigoOrigen,reo.ess as essOrigen,reo.distrito as distritoOrigen,reo.provincia as provinciaOrigen, co.renaesDestino as codigoDestino,
            rede.ess as essDestino,rede.distrito as distritoDestino,rede.provincia as provinciaDestino,ci1.codigo as codigoCi1,ci1.descripcion as descripcionC1,
            ci2.codigo as codigoCi2,ci2.descripcion as descripcionC2,ci3.codigo as codigoCi3,ci3.descripcion as descripcionC3,ci4.codigo as codigoCi4,ci4.descripcion as descripcionC4,gru.descripcion as grupoRiesgo,
            co.fechaInicio as fechaInicio,co.fechaIngreso as fechaIngreso, co.horaInicio as horaInicio,co.horaIngreso as horaDeIngreso,co.responsable responsable,p.dni,p.apellidoPaterno,p.apellidoMaterno,p.nombres,
            p.direccion,p.fechanacimiento,co.g,co.p1,co.p2,co.p3,co.p4,co.eg,co.cpn,co.periodo,co.estado,co.comentarios,co.fechaEgreso as fechaEgreso,co.horaEgreso as horaDeEgreso
            from control co
            inner join paciente p on co.paciente=p.dni
            inner join renaes reo on co.renaesOrigen=reo.codigo
            inner join renaes rede on co.renaesDestino=rede.codigo
            inner join cie ci1 on co.cie1=ci1.codigo
            inner join cie ci2 on co.cie2=ci2.codigo
            inner join cie ci3 on co.cie3=ci3.codigo
            inner join cie ci4 on co.cie4=ci4.codigo
            inner join gruporiesgo gru on co.grupoRiesgo=gru.codigo";
			$resultado = $db->query($sql);
			return $resultado;
        }

        public function mostrarHospitalizados()
        {
            $db= new Conexion();
            $sql="SELECT co.codigo as codigoControl, CONCAT(p.apellidoPaterno,' ',p.apellidoMaterno,' ',p.nombres) as ppaciente,co.fechaIngreso as fechaIngreso,co.horaIngreso as horaDeIngreso,co.serie as codigoSerie,co.red as codigoRed,
            co.renaesOrigen as codigoOrigen,reo.ess as essOrigen,reo.distrito as distritoOrigen,reo.provincia as provinciaOrigen, co.renaesDestino as codigoDestino,
            rede.ess as essDestino,rede.distrito as distritoDestino,rede.provincia as provinciaDestino,ci1.codigo as codigoCi1,ci1.descripcion as descripcionC1,
            ci2.codigo as codigoCi2,ci2.descripcion as descripcionC2,ci3.codigo as codigoCi3,ci3.descripcion as descripcionC3,ci4.codigo as codigoCi4,ci4.descripcion as descripcionC4,gru.descripcion as grupoRiesgo,
            co.fechaInicio as fechaInicio,co.fechaIngreso as fechaIngreso, co.horaInicio as horaInicio,co.horaIngreso as horaDeIngreso,co.responsable responsable,p.dni,p.apellidoPaterno,p.apellidoMaterno,p.nombres,
            p.direccion,p.fechanacimiento,co.g,co.p1,co.p2,co.p3,co.p4,co.eg,co.cpn,co.periodo,co.estado,co.comentarios,co.fechaEgreso as fechaEgreso,co.horaEgreso as horaDeEgreso
            from control co
            inner join paciente p on co.paciente=p.dni
            inner join renaes reo on co.renaesOrigen=reo.codigo
            inner join renaes rede on co.renaesDestino=rede.codigo
            inner join cie ci1 on co.cie1=ci1.codigo
            inner join cie ci2 on co.cie2=ci2.codigo
            inner join cie ci3 on co.cie3=ci3.codigo
            inner join cie ci4 on co.cie4=ci4.codigo
            inner join gruporiesgo gru on co.grupoRiesgo=gru.codigo
            where co.fechaEgreso = '0000-00-00'";
           $resultado=$db->query($sql);
           return $resultado;
        }

        public function fitltrarHospitalizados($dia_inicio, $dia_fin)
        {
            $db= new Conexion();
            $sql="SELECT co.codigo as codigoControl, CONCAT(p.apellidoPaterno,' ',p.apellidoMaterno,' ',p.nombres) as ppaciente,co.fechaIngreso as fechaIngreso,co.horaIngreso as horaDeIngreso,co.serie as codigoSerie,co.red as codigoRed,
            co.renaesOrigen as codigoOrigen,reo.ess as essOrigen,reo.distrito as distritoOrigen,reo.provincia as provinciaOrigen, co.renaesDestino as codigoDestino,
            rede.ess as essDestino,rede.distrito as distritoDestino,rede.provincia as provinciaDestino,ci1.codigo as codigoCi1,ci1.descripcion as descripcionC1,
            ci2.codigo as codigoCi2,ci2.descripcion as descripcionC2,ci3.codigo as codigoCi3,ci3.descripcion as descripcionC3,ci4.codigo as codigoCi4,ci4.descripcion as descripcionC4,gru.descripcion as grupoRiesgo,
            co.fechaInicio as fechaInicio,co.fechaIngreso as fechaIngreso, co.horaInicio as horaInicio,co.horaIngreso as horaDeIngreso,co.responsable responsable,p.dni,p.apellidoPaterno,p.apellidoMaterno,p.nombres,
            p.direccion,p.fechanacimiento,co.g,co.p1,co.p2,co.p3,co.p4,co.eg,co.cpn,co.periodo,co.estado,co.comentarios,co.fechaEgreso as fechaEgreso,co.horaEgreso as horaDeEgreso
            from control co
            inner join paciente p on co.paciente=p.dni
            inner join renaes reo on co.renaesOrigen=reo.codigo
            inner join renaes rede on co.renaesDestino=rede.codigo
            inner join cie ci1 on co.cie1=ci1.codigo
            inner join cie ci2 on co.cie2=ci2.codigo
            inner join cie ci3 on co.cie3=ci3.codigo
            inner join cie ci4 on co.cie4=ci4.codigo
            inner join gruporiesgo gru on co.grupoRiesgo=gru.codigo
            where co.fechaEgreso = '0000-00-00' and co.fechaInicio BETWEEN '".$dia_inicio."' and '".$dia_fin."'";
           $resultado=$db->query($sql);
           return $resultado;
        }


        public function mostrarEgresos()
        {
            $db= new Conexion();
            $sql="SELECT co.codigo as codigoControl, CONCAT(p.apellidoPaterno,' ',p.apellidoMaterno,' ',p.nombres) as ppaciente,co.fechaIngreso as fechaIngreso,co.horaIngreso as horaDeIngreso,co.serie as codigoSerie,co.red as codigoRed,
            co.renaesOrigen as codigoOrigen,reo.ess as essOrigen,reo.distrito as distritoOrigen,reo.provincia as provinciaOrigen, co.renaesDestino as codigoDestino,
            rede.ess as essDestino,rede.distrito as distritoDestino,rede.provincia as provinciaDestino,ci1.codigo as codigoCi1,ci1.descripcion as descripcionC1,
            ci2.codigo as codigoCi2,ci2.descripcion as descripcionC2,ci3.codigo as codigoCi3,ci3.descripcion as descripcionC3,ci4.codigo as codigoCi4,ci4.descripcion as descripcionC4,gru.descripcion as grupoRiesgo,
            co.fechaInicio as fechaInicio,co.fechaIngreso as fechaIngreso, co.horaInicio as horaInicio,co.horaIngreso as horaDeIngreso,co.responsable responsable,p.dni,p.apellidoPaterno,p.apellidoMaterno,p.nombres,
            p.direccion,p.fechanacimiento,co.g,co.p1,co.p2,co.p3,co.p4,co.eg,co.cpn,co.periodo,co.estado,co.comentarios,co.fechaEgreso as fechaEgreso,co.horaEgreso as horaDeEgreso
            from control co
            inner join paciente p on co.paciente=p.dni
            inner join renaes reo on co.renaesOrigen=reo.codigo
            inner join renaes rede on co.renaesDestino=rede.codigo
            inner join cie ci1 on co.cie1=ci1.codigo
            inner join cie ci2 on co.cie2=ci2.codigo
            inner join cie ci3 on co.cie3=ci3.codigo
            inner join cie ci4 on co.cie4=ci4.codigo
            inner join gruporiesgo gru on co.grupoRiesgo=gru.codigo
            where co.fechaEgreso != '0000-00-00'";
           $resultado=$db->query($sql);
           return $resultado;
        }

        public function filtrarFechas($dia_inicio, $dia_fin)
        {
            $db= new Conexion();
            $sql="SELECT co.codigo as codigoControl, CONCAT(p.apellidoPaterno,' ',p.apellidoMaterno,' ',p.nombres) as ppaciente,co.fechaIngreso as fechaIngreso,co.horaIngreso as horaDeIngreso,co.serie as codigoSerie,co.red as codigoRed,
            co.renaesOrigen as codigoOrigen,reo.ess as essOrigen,reo.distrito as distritoOrigen,reo.provincia as provinciaOrigen, co.renaesDestino as codigoDestino,
            rede.ess as essDestino,rede.distrito as distritoDestino,rede.provincia as provinciaDestino,ci1.codigo as codigoCi1,ci1.descripcion as descripcionC1,
            ci2.codigo as codigoCi2,ci2.descripcion as descripcionC2,ci3.codigo as codigoCi3,ci3.descripcion as descripcionC3,ci4.codigo as codigoCi4,ci4.descripcion as descripcionC4,gru.descripcion as grupoRiesgo,
            co.fechaInicio as fechaInicio,co.fechaIngreso as fechaIngreso, co.horaInicio as horaInicio,co.horaIngreso as horaDeIngreso,co.responsable responsable,p.dni,p.apellidoPaterno,p.apellidoMaterno,p.nombres,
            p.direccion,p.fechanacimiento,co.g,co.p1,co.p2,co.p3,co.p4,co.eg,co.cpn,co.periodo,co.estado,co.comentarios,co.fechaEgreso as fechaEgreso,co.horaEgreso as horaDeEgreso
            from control co
            inner join paciente p on co.paciente=p.dni
            inner join renaes reo on co.renaesOrigen=reo.codigo
            inner join renaes rede on co.renaesDestino=rede.codigo
            inner join cie ci1 on co.cie1=ci1.codigo
            inner join cie ci2 on co.cie2=ci2.codigo
            inner join cie ci3 on co.cie3=ci3.codigo
            inner join cie ci4 on co.cie4=ci4.codigo
            inner join gruporiesgo gru on co.grupoRiesgo=gru.codigo
            where co.fechaInicio BETWEEN '".$dia_inicio."' and '".$dia_fin."'";
			$resultado = $db->query($sql);
			return $resultado;
        }

        

    }



?>