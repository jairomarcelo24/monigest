<?php

    include_once "conexion.php";
    class Paciente
    {
        private $dni;
        private $apellidoP;
        private $apellidoM;
        private $nombres;
        private $direccion;
        private $fechanacimiento;

        function __construct($dni,$apellidoP,$apellidoM,$nombres,$direccion,$fechanacimiento)
        {
            $this->dni=$dni;
            $this->apellidoP=$apellidoP;
            $this->apellidoM=$apellidoM;
            $this->nombres=$nombres;
            $this->direccion=$direccion;
            $this->fechanacimiento=$fechanacimiento;
        }

        public static function constructorvacio()
        {
            return new self('','','','','','');
        }

        public function insertarPaciente()
		{
			$db= new Conexion();
			$sql = "INSERT INTO paciente(dni, apellidoPaterno, apellidoMaterno, nombres, direccion,fechanacimiento) VALUES ('$this->dni', '$this->apellidoP','$this->apellidoM' ,'$this->nombres','$this->direccion','$this->fechanacimiento')";
			$db->query($sql) ? $mm = "registro correcto"  : $mm = "no se puede registrar :: ENTIDAD";
			echo $mm;
        }
        
        public function buscarPaciente($dni)
		{
			$db= new Conexion();
			$sql="SELECT dni, apellidoPaterno, apellidoMaterno, nombres, direccion,fechanacimiento FROM paciente WHERE dni = '$dni' ";
			$resultado = $db->query($sql);
			return $resultado;
        }
        
        public function mostrarPacientes()
		{
			$db= new Conexion();
			$sql="SELECT dni, apellidoPaterno, apellidoMaterno, nombres, direccion,fechanacimiento FROM paciente";
			$resultado = $db->query($sql);
			return $resultado;
        }
        
        /*public function ultimoPaciente()
        {
            $db=new Conexion();
            $sql="select max(codigo) as ultimo from paciente";
            $resultado = $db->query($sql);
            return $resultado;
        }*/

        public function buscarRepetidoPaciente($dni)
        {
            $db= new Conexion();
            $sql="SELECT dni FROM paciente WHERE dni='$dni'";
            $resultado= $db->query($sql);
            return $resultado;
        }

        public function listarPacientes()
        {
            $db = new Conexion();
            $sql = "SELECT dni, apellidoPaterno, apellidoMaterno, nombres, direccion, fechanacimiento FROM paciente";
            $resultado = $db->query($sql);
            return $resultado;
        }

        public function contarPacientes(){
            $db = new Conexion();
            $sql = " SELECT COUNT(*) as cantidad from paciente";
            $resultado = $db->query($sql);
            return $resultado;
        }

    }

?>