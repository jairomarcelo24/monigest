<?php

    include_once "conexion.php";
    class GrupoRiesgo
    {
        private $codigo;
        private $descripcion;

        function __construct($codigo,$descripcion)
        {
            $this->codigo=$codigo;
            $this->descripcion=$descripcion;
        }

        public static function constructorvacio()
        {
            return new self('','');
        }

        public function mostrarGrupo()
		{
			$db= new Conexion();
			$sql="SELECT codigo, descripcion FROM gruporiesgo";
			$resultado = $db->query($sql);
			return $resultado;
		}

    }

?>