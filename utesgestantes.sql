-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 07-01-2019 a las 06:12:34
-- Versión del servidor: 10.1.26-MariaDB
-- Versión de PHP: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `utesgestantes`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cie`
--

CREATE TABLE `cie` (
  `codigo` varchar(10) NOT NULL,
  `descripcion` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cie`
--

INSERT INTO `cie` (`codigo`, `descripcion`) VALUES
('0', 'NO PRESENTA'),
('10060', 'DEBRIDACION DE HERIDAS Y ABSCESOS'),
('10120', 'EXTRACCION DE CUERPO EXTRANIO'),
('10781', 'CANALIZACION DE VIA PERIFERICA'),
('11100', 'TOMAS DE BIOSPSIA EN PIEL, TEJIDO SUBCUTANEO Y/O '),
('11400', 'EXERESIS DE LESION BENIGNA'),
('11443', 'EXTRACCION DE QUISTES MENORES'),
('11750', 'EXTRACCION DE UÃ‘A'),
('12002', 'SUTURA DE HERIDA SUPERFICIAL DE PIEL');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `control`
--

CREATE TABLE `control` (
  `cie1` varchar(10) NOT NULL,
  `cie2` varchar(10) NOT NULL,
  `cie3` varchar(10) NOT NULL,
  `cie4` varchar(10) NOT NULL,
  `codigo` int(11) NOT NULL,
  `paciente` varchar(8) NOT NULL,
  `comentarios` varchar(200) NOT NULL,
  `cpn` varchar(5) NOT NULL,
  `eg` varchar(5) NOT NULL,
  `estado` varchar(20) NOT NULL,
  `fechaEgreso` date NOT NULL,
  `horaEgreso` time NOT NULL,
  `fechaIngreso` date NOT NULL,
  `horaIngreso` time NOT NULL,
  `fechaInicio` date NOT NULL,
  `horaInicio` time NOT NULL,
  `g` varchar(5) NOT NULL,
  `grupoRiesgo` int(11) NOT NULL,
  `p1` varchar(5) NOT NULL,
  `p2` varchar(5) NOT NULL,
  `p3` varchar(5) NOT NULL,
  `p4` varchar(5) NOT NULL,
  `periodo` varchar(20) NOT NULL,
  `red` varchar(50) NOT NULL,
  `renaesDestino` varchar(20) NOT NULL,
  `renaesOrigen` varchar(20) NOT NULL,
  `responsable` varchar(50) NOT NULL,
  `serie` varchar(10) NOT NULL,
  `usuario` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `control`
--

INSERT INTO `control` (`cie1`, `cie2`, `cie3`, `cie4`, `codigo`, `paciente`, `comentarios`, `cpn`, `eg`, `estado`, `fechaEgreso`, `horaEgreso`, `fechaIngreso`, `horaIngreso`, `fechaInicio`, `horaInicio`, `g`, `grupoRiesgo`, `p1`, `p2`, `p3`, `p4`, `periodo`, `red`, `renaesDestino`, `renaesOrigen`, `responsable`, `serie`, `usuario`) VALUES
('12002', '10060', '0', '0', 1, '8784541', 'BUEN ESTADO DE SALUD', '9', '41', 'ESTABLE', '2018-07-20', '20:00:00', '2018-07-06', '10:00:00', '2018-07-05', '04:01:00', '2', 1, '1', '1', '1', '2', 'PARTO', 'RED CHEPEN', '5198', '5200', 'KATY PEREZ', '28', '12121212'),
('10120', '0', '0', '0', 2, '45125451', 'TODO ESTA BIEN', '9', '41', 'ESTABLE', '0000-00-00', '00:00:00', '2018-12-06', '10:00:00', '2018-12-05', '12:00:00', '2', 5, '1', '1', '1', '2', 'PARTO', 'RED ASCOPE', '5207', '5202', 'KATY PEREZ', '2', '12121212'),
('11750', '0', '0', '0', 3, '87451256', 'COMPLICADO', '9', '41', 'RESERVADO', '0000-00-00', '00:00:00', '2018-12-07', '16:00:00', '2018-12-07', '15:00:00', '1', 1, '1', '1', '1', '2', 'EMBARAZO', 'RED PACASMAYO', '5199', '5199', 'KATY PEREZ', '5', '12121212'),
('0', '0', '0', '0', 4, '54512000', 'COMPLICACIONES EN EL PARTO', '9', '41', 'RESERVADO', '2018-12-10', '20:00:00', '2018-12-05', '13:00:00', '2018-12-04', '12:00:00', '1', 1, '1', '1', '1', '2', 'PARTO', 'RED CHEPEN', '5198', '5199', 'KATY PEREZ', '21', '12121212'),
('10781', '11400', '0', '0', 5, '70500261', 'NO TIENE COMPLICACION', '9', '41', 'RESERVADO', '2018-11-18', '15:00:00', '2018-11-16', '10:00:00', '2018-11-02', '20:00:00', '2', 4, '1', '2', '1', '2', 'PARTO', 'RED TRUJILLO', '5202', '5200', 'KATY PEREZ', '15', '12121212');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `gruporiesgo`
--

CREATE TABLE `gruporiesgo` (
  `codigo` int(11) NOT NULL,
  `descripcion` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `gruporiesgo`
--

INSERT INTO `gruporiesgo` (`codigo`, `descripcion`) VALUES
(1, 'ENFERMEDADES INFECCIOSAS INTESTINALES'),
(2, 'TRASTORNOS EPISODICOS Y PAROXISTICOS'),
(3, 'TRASTORNOS DE LOS NERVIOS, DE LAS RAICES Y DE LOS PLEXOS NERVIOSOS'),
(4, 'POLINEUROPATIAS Y OTROS TRASTORNOS DEL SISTEMA NERVIOSO PERIFERICO'),
(5, 'ENFERMEDADES MUSCULARES Y DE LA UNION NEUROMUSCULAR'),
(6, 'PARALISIS CEREBRAL Y OTROS SINDROMES PARALITICOS'),
(7, 'OTROS TRASTORNOS DEL SISTEMA NERVIOSO');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `paciente`
--

CREATE TABLE `paciente` (
  `dni` varchar(8) NOT NULL,
  `apellidoPaterno` varchar(30) NOT NULL,
  `apellidoMaterno` varchar(30) NOT NULL,
  `nombres` varchar(50) NOT NULL,
  `direccion` varchar(30) NOT NULL,
  `fechanacimiento` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `paciente`
--

INSERT INTO `paciente` (`dni`, `apellidoPaterno`, `apellidoMaterno`, `nombres`, `direccion`, `fechanacimiento`) VALUES
('45125451', 'ALCANTARA', 'PEREZ', 'JUANA', 'SABOGAL 451 - URB PALERMO', '2000-01-01'),
('54512000', 'LLAURE', 'REYNA', 'MICAELA', 'ARANJUEZ 451', '1999-10-31'),
('70500261', 'MARCELO', 'RUIZ', 'IVVONNE', 'JOSE SABOGAL ', '2000-01-06'),
('87451256', 'LOPEZ', 'OBRADOR', 'LETICIA', 'UNION 451', '1998-02-04'),
('8784541', 'PRUEBA', 'EJEMPLO', 'YAHAIRA', 'SANTA INES', '1999-12-09');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `renaes`
--

CREATE TABLE `renaes` (
  `codigo` varchar(20) NOT NULL,
  `ess` varchar(100) NOT NULL,
  `distrito` varchar(100) NOT NULL,
  `provincia` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `renaes`
--

INSERT INTO `renaes` (`codigo`, `ess`, `distrito`, `provincia`) VALUES
('11007', 'EL TROPICO', 'HUANCHACO', 'TRUJILLO'),
('11629', 'HUANCHAQUITO', 'HUANCHACO', 'TRUJILLO'),
('12228', 'VICTOR RAUL HAYA DE LA TORRE', 'LA ESPERANZA', 'TRUJILLO'),
('12229', 'BARRIO 1', 'EL PORVENIR', 'TRUJILLO'),
('13242', 'SUPERVIVENCIA', 'EL PORVENIR', 'TRUJILLO'),
('17752', 'LA CABAÑA', 'FLORENCIA DE MORA', 'TRUJILLO'),
('5195', 'HOSPITAL BELEN DE TRUJILLO', 'TRUJILLO', 'TRUJILLO'),
('5196', 'REGIONAL DOCENTE DE TRUJILLO', 'TRUJILLO', 'TRUJILLO'),
('5198', 'SAN MARTIN PORRES', 'TRUJILLO', 'TRUJILLO'),
('5199', 'LA UNION', 'TRUJILLO', 'TRUJILLO'),
('5200', 'LOS JARDINES', 'TRUJILLO', 'TRUJILLO'),
('5201', 'ARANJUEZ', 'TRUJILLO', 'TRUJILLO'),
('5202', 'EL BOSQUE', 'TRUJILLO', 'TRUJILLO'),
('5203', 'SAGRADO CORAZON', 'TRUJILLO', 'TRUJILLO'),
('5204', 'LA NORIA', 'TRUJILLO', 'TRUJILLO'),
('5205', 'PESQUEDA II', 'TRUJILLO', 'TRUJILLO'),
('5206', 'LIBERTAD', 'TRUJILLO', 'TRUJILLO'),
('5207', 'PESQUEDA III', 'TRUJILLO', 'TRUJILLO'),
('5208', 'CLUB DE LEONES', 'TRUJILLO', 'TRUJILLO'),
('5209', 'SANTA ISABEL', 'EL PORVENIR', 'TRUJILLO'),
('5210', 'BUEN PASTOR', 'EL PORVENIR', 'TRUJILLO'),
('5211', 'GRAN CHIMU', 'EL PORVENIR', 'TRUJILLO'),
('5212', 'MIGUEL GRAU', 'EL PORVENIR', 'TRUJILLO'),
('5213', 'RIO SECO - SANTA ROSA', 'EL PORVENIR', 'TRUJILLO'),
('5214', 'VICTOR RAUL HAYA DE LA TORRE', 'EL PORVENIR', 'TRUJILLO'),
('5215', 'INDOAMERICA', 'EL PORVENIR', 'TRUJILLO'),
('5216', 'VIRGEN DEL CARMEN', 'EL PORVENIR', 'TRUJILLO'),
('5217', 'EL ESFUERZO', 'FLORENCIA DE MORA', 'TRUJILLO'),
('5218', 'FLORENCIA DE MORA PARTE ALTA', 'FLORENCIA DE MORA', 'TRUJILLO'),
('5219', 'SANTO TORIBIO DE MOGROVEJO', 'FLORENCIA DE MORA', 'TRUJILLO'),
('5220', 'ALTO TRUJILLO', 'EL PORVENIR', 'TRUJILLO'),
('5221', 'HUANCHACO', 'HUANCHACO', 'TRUJILLO'),
('5222', 'EL MILAGRO', 'LA ESPERANZA', 'TRUJILLO'),
('5223', 'VILLA DEL MAR', 'HUANCHACO', 'TRUJILLO'),
('5225', 'SANTISIMO SACRAMENTO', 'LA ESPERANZA', 'TRUJILLO'),
('5226', 'JERUSALEN', 'LA ESPERANZA', 'TRUJILLO'),
('5227', 'SAN MARTIN', 'LA ESPERANZA', 'TRUJILLO'),
('5228', 'PUEBLO LIBRE', 'LA ESPERANZA', 'TRUJILLO'),
('5229', 'BELLAVISTA', 'LA ESPERANZA', 'TRUJILLO'),
('5230', 'WICHANZAO', 'LA ESPERANZA', 'TRUJILLO'),
('5231', 'HOSPITAL DISTRITAL DE LAREDO', 'LAREDO', 'TRUJILLO'),
('5232', 'MENOCUCHO', 'LAREDO', 'TRUJILLO'),
('5233', 'SANTO DOMINGO', 'LAREDO', 'TRUJILLO'),
('5234', 'SANTA LUCIA DE MOCHE', 'MOCHE', 'TRUJILLO'),
('5235', 'ALTO MOCHE', 'MOCHE', 'TRUJILLO'),
('5236', 'ELIO JACOBBO CAFFO', 'MOCHE ', 'TRUJILLO'),
('5237', 'SAN PEDRO - DELICIAS', 'MOCHE', 'TRUJILLO'),
('5238', 'POROTO', 'LAREDO', 'TRUJILLO'),
('5239', 'SALAVERRY', 'SALAVERRY', 'TRUJILLO'),
('5240', 'AURORA DIAZ', 'SALAVERRY', 'TRUJILLO'),
('5241', 'SIMBAL', 'LAREDO', 'TRUJILLO'),
('5242', 'VICTOR LARCO HERRERA', 'VICTOR LARCO HERRERA', 'TRUJILLO'),
('5243', 'VISTA ALEGRE', 'VICTOR LARCO HERRERA', 'TRUJILLO'),
('5244', 'BUENOS AIRES SUR', 'VICTOR LARCO HERRERA', 'TRUJILLO'),
('5245', 'LIBERACION SOCIAL', 'VICTOR LARCO HERRERA', 'TRUJILLO'),
('5246', 'HUAMAN', 'VICTOR LARCO HERRERA', 'TRUJILLO'),
('7115', 'MANUEL AREVALO', 'LA ESPERANZA', 'TRUJILLO'),
('7151', 'CURVA DE SUN', 'MOCHE', 'TRUJILLO'),
('7152', 'WALTER CRUZ VILCA', 'MOCHE', 'TRUJILLO');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `dni` varchar(20) NOT NULL,
  `apellidoPaterno` varchar(30) NOT NULL,
  `apellidoMaterno` varchar(30) NOT NULL,
  `nombres` varchar(30) NOT NULL,
  `password` varchar(150) NOT NULL,
  `telefono` varchar(30) NOT NULL,
  `tipo` varchar(30) NOT NULL,
  `establecimiento` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`dni`, `apellidoPaterno`, `apellidoMaterno`, `nombres`, `password`, `telefono`, `tipo`, `establecimiento`) VALUES
('10101010', 'NEIRA', 'RUIZ', 'ULISES', '$2y$10$0E7XvvrDTIfvhrIpns6SL.xVPZz.Tm1Mq.7irfj6uIvZTFcgvowoi', '9998989898', 'USUARIO', 'HOSPITAL BELEN'),
('12121212', 'GARMENDIA', 'REYNA', 'ISIS', '$2y$10$gBdxYS5JYtNypbs0kPoS1ulYoA2D9ynC2aq39HULWfkIaFcrDAgom', '42142142412', 'ADMINISTRADOR', 'LA NORIA'),
('12345678', 'Neira', 'Saldaña', 'Maria', '654321', '78459658', 'USUARIO', 'HOSPITAL BELEN'),
('54654654654', 'Ruiz', 'Pretell', 'PRUEBA', '$2y$10$3KJN/5XaC.ku3szxcxryxebXO9Vdb7PuOIlmdEZHRPeNg3oyQ7WMq', '54165', 'ADMINISTRADOR', 'HOSPITAL BELEN'),
('55555454', 'PRUEBA', 'Pretell', 'Jairo', '$2y$10$j/hFeWpoMegurvFEKWIkjuZjK4yoD1LPS7lwrLmlyacR3nwkxlmdm', '24124', 'USUARIO', 'HOSPITAL BELEN'),
('55555558', 'PRUEBA', 'MENU', 'MENU', '123456', '5555552212', 'administrador', 'utes'),
('70500260', 'Marcelo', 'Ruiz', 'Jairo', '$2y$10$Lrgun/1KxRijKtQ2a0/YvOmUhpoHMRxNBNJskqr.vUfmnTeaCtB7i', '99999999', 'ADMINISTRADOR', 'LA NORIA'),
('70500261', 'Marcelo', 'Ruiz', 'Jairo', '123456', '999999999', 'ADMINISTRADOR', 'La Noria'),
('88888888', 'NUEVO', 'NUEVO', 'NUEVO', '$2y$10$wuEJaAbolugBzqzTkzsBPeYDDCB.NtofxmMlTJ2Yvk2WODXIXuOum', '89589556', 'USUARIO', 'HOSPITAL BELEN');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `cie`
--
ALTER TABLE `cie`
  ADD PRIMARY KEY (`codigo`);

--
-- Indices de la tabla `control`
--
ALTER TABLE `control`
  ADD PRIMARY KEY (`codigo`),
  ADD KEY `usuario` (`usuario`),
  ADD KEY `renaesOrigen` (`renaesOrigen`),
  ADD KEY `renaesDestino` (`renaesDestino`),
  ADD KEY `cie1` (`cie1`),
  ADD KEY `cie2` (`cie2`),
  ADD KEY `cie3` (`cie3`),
  ADD KEY `cie4` (`cie4`),
  ADD KEY `grupoRiesgo` (`grupoRiesgo`),
  ADD KEY `paciente` (`paciente`),
  ADD KEY `paciente_2` (`paciente`);

--
-- Indices de la tabla `gruporiesgo`
--
ALTER TABLE `gruporiesgo`
  ADD PRIMARY KEY (`codigo`);

--
-- Indices de la tabla `paciente`
--
ALTER TABLE `paciente`
  ADD PRIMARY KEY (`dni`);

--
-- Indices de la tabla `renaes`
--
ALTER TABLE `renaes`
  ADD PRIMARY KEY (`codigo`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`dni`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `control`
--
ALTER TABLE `control`
  MODIFY `codigo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `gruporiesgo`
--
ALTER TABLE `gruporiesgo`
  MODIFY `codigo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `control`
--
ALTER TABLE `control`
  ADD CONSTRAINT `control_ibfk_1` FOREIGN KEY (`cie1`) REFERENCES `cie` (`codigo`) ON UPDATE CASCADE,
  ADD CONSTRAINT `control_ibfk_10` FOREIGN KEY (`paciente`) REFERENCES `paciente` (`dni`) ON UPDATE CASCADE,
  ADD CONSTRAINT `control_ibfk_2` FOREIGN KEY (`grupoRiesgo`) REFERENCES `gruporiesgo` (`codigo`) ON UPDATE CASCADE,
  ADD CONSTRAINT `control_ibfk_3` FOREIGN KEY (`renaesOrigen`) REFERENCES `renaes` (`codigo`) ON UPDATE CASCADE,
  ADD CONSTRAINT `control_ibfk_4` FOREIGN KEY (`cie2`) REFERENCES `cie` (`codigo`) ON UPDATE CASCADE,
  ADD CONSTRAINT `control_ibfk_5` FOREIGN KEY (`cie3`) REFERENCES `cie` (`codigo`) ON UPDATE CASCADE,
  ADD CONSTRAINT `control_ibfk_6` FOREIGN KEY (`cie4`) REFERENCES `cie` (`codigo`) ON UPDATE CASCADE,
  ADD CONSTRAINT `control_ibfk_7` FOREIGN KEY (`renaesDestino`) REFERENCES `renaes` (`codigo`) ON UPDATE CASCADE,
  ADD CONSTRAINT `control_ibfk_9` FOREIGN KEY (`usuario`) REFERENCES `usuario` (`dni`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
