$(buscar());

function buscar(dia_inicio, dia_fin){
    $.ajax({
        url:  'controles/graficoServicio.php',
        type:'POST',
        dataType: 'html',
        data:{dia_inicio: dia_inicio, dia_fin: dia_fin},
    }).done(function(respuesta){
       console.log(respuesta);
        $("#grafico").html(respuesta);
    }).fail(function(){
        console.log("error");
    })
}


$('#filtrar').click(function(){
    var inicio = $('#dia_inicio').val();
    var fin = $('#dia_fin').val();
    if(inicio != "" || fin !="" ){
        buscar(inicio, fin);
    }else{
        buscar();
    }

});