<?php
  session_start();
  if(!isset($_SESSION['usuario'])){
    header("Location: index.php");
 }

 /*if(isset($_GET["parametro"])){
 $codigoControl=$_GET["parametro"];
 }*/
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Gestantes Utes </title>

    <!-- Bootstrap -->
    <link href="vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- bootstrap-datetimepicker -->
    <link href="vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css" rel="stylesheet">
    <!-- Custom Theme Style -->
    <link href="build/css/custom.min.css" rel="stylesheet">
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="home.php" class="site_title"><i class="fa fa-paw"></i> <span>MONIGEST</span></a>
            </div>

            <div class="clearfix"></div>
           <!-- INCLUIMOS EL MENU -->
           <?php include "menu.php"; ?>
           <!-- FIN DE MENU -->

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="page-title">
              <div class="title_left">  
                <h3>Registro de Control</h3>
              </div>
            </div>
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Control<small></small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <form action="controles/controlservicio.php" method="POST" class="form-horizontal form-label-left" novalidate>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" >Numero de Referencia <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="serie" class="form-control col-md-7 col-xs-12" name="serie" placeholder="" required="required" type="text">
                        </div>
                      </div>
                      
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" >Dni del Paciente<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <?php if(isset($_GET["dnip"])){
                              $pacienteDni=$_GET["dnip"]; ?>
                          <input id="dni" class="form-control col-md-7 col-xs-12"  name="" value="<?php echo $pacienteDni ?>" required="required" type="text" disabled >
                          <input type="hidden" value="<?php echo $pacienteDni ?>" name="dni">
                            <?php }else{ ?>
                              <input id="dni" class="form-control col-md-7 col-xs-12"  name="dni" value="" required="required" type="text"  >        
                            <?php } ?>
                        </div>
                      </div>

                        <!-- DATOS PROPIOS DEL CONTROL -->
                      
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" >Fecha y Hora de Inicio de Complicación <span class="required">*</span>
                        </label>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                          <input id="fechaInicio" class="form-control col-md-7 col-xs-12" name="fechaInicio"  required="required" type="date">
                        </div> 
                        <div  class="col-md-2 col-sm-6 col-xs-12">
                        <input type="time" name="horaInicio" step="" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Red que reporta</label>
                        <div class="col-md-6 col-sm-9 col-xs-12">
                          <select class="form-control" name="cboRed">
                            <option value="NO TIENE ASIGNADO NINGUNA RED">NO TIENE ASIGNADO NINGUNA RED</option>
                            <option value="RED ASCOPE">RED ASCOPE</option>
                            <option value="RED BOLIVAR">RED BOLIVAR</option>
                            <option value="RED CHEPEN">RED CHEPEN</option>
                            <option value="RED GRAN CHIMU">RED GRAN CHIMU</option>
                            <option value="RED JULCAN">RED JULCAN</option>
                            <option value="RED OTUZCO">RED OTUZCO</option>
                            <option value="RED PACASMAYO">RED PACASMAYO</option>
                            <option value="RED PATAZ">RED PATAZ</option>
                            <option value="RED SANCHEZ CARRION">RED SANCHEZ CARRION</option>
                            <option value="RED SANTIAGO DE CHUCO">RED SANTIAGO DE CHUCO</option>
                            <option value="RED TRUJILLO">RED TRUJILLO</option>
                            <option value="RED VIRU">RED VIRU</option>
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Renaes Origen</label>
                        <div class="col-md-6 col-sm-9 col-xs-12">
                          <select class="form-control" name="cborenaesOrigen">
                          <?php include_once "entidades/renaes.php" ;
                          $renaes= Renaes::constructorvacio();
                          $datos=$renaes->mostrarRenaes(); 
                          while( $fila = $datos->fetch_array()){ ?>
                            <option value=<?php echo $fila['codigo']?>><?php echo $fila['codigo']." - ".$fila['ess']." - ".$fila['distrito']." - ".$fila['provincia']?></option>
                          <?php } ?>
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Renaes Destino</label>
                        <div class="col-md-6 col-sm-9 col-xs-12">
                          <select class="form-control" name="cborenaesDestino">
                          <?php include_once "entidades/renaes.php" ;
                          $renaes= Renaes::constructorvacio();
                          $datos=$renaes->mostrarRenaes(); 
                          while( $fila = $datos->fetch_array()){ ?>
                            <option value=<?php echo $fila['codigo']?>><?php echo $fila['codigo']." - ".$fila['ess']." - ".$fila['distrito']." - ".$fila['provincia']?></option>
                          <?php } ?>
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">CIE-01</label>
                        <div class="col-md-6 col-sm-9 col-xs-12">
                          <select class="form-control" name="cbocie01">
                          <?php include_once "entidades/cie.php" ;
                          $cie= Cie::constructorvacio();
                          $datos=$cie->mostrarCie(); 
                          while( $fila = $datos->fetch_array()){ ?>
                            <option value=<?php echo $fila['codigo']?>><?php echo  $fila['codigo']." - ".$fila['descripcion'] ?></option>
                          <?php } ?>
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">CIE-02</label>
                        <div class="col-md-6 col-sm-9 col-xs-12">
                          <select class="form-control" name="cbocie02">
                          <?php include_once "entidades/cie.php" ;
                          $cie= Cie::constructorvacio();
                          $datos=$cie->mostrarCie(); 
                          while( $fila = $datos->fetch_array()){ ?>
                            <option value=<?php echo $fila['codigo']?>><?php echo  $fila['codigo']." - ".$fila['descripcion'] ?></option>
                          <?php } ?>
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">CIE-03</label>
                        <div class="col-md-6 col-sm-9 col-xs-12">
                          <select class="form-control" name="cbocie03">
                          <?php include_once "entidades/cie.php" ;
                          $cie= Cie::constructorvacio();
                          $datos=$cie->mostrarCie(); 
                          while( $fila = $datos->fetch_array()){ ?>
                            <option value=<?php echo $fila['codigo']?>><?php echo  $fila['codigo']." - ".$fila['descripcion'] ?></option>
                          <?php } ?>
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">CIE-04</label>
                        <div class="col-md-6 col-sm-9 col-xs-12">
                          <select class="form-control" name="cbocie04">
                          <?php include_once "entidades/cie.php" ;
                          $cie= Cie::constructorvacio();
                          $datos=$cie->mostrarCie(); 
                          while( $fila = $datos->fetch_array()){ ?>
                            <option value=<?php echo $fila['codigo']?>><?php echo  $fila['codigo']." - ".$fila['descripcion'] ?></option>
                          <?php } ?>
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Grupo de Riesgo</label>
                        <div class="col-md-6 col-sm-9 col-xs-12">
                          <select class="form-control" name="cbogrupoRiesgo">
                          <?php include_once "entidades/gruporiesgo.php" ;
                          $grupo= GrupoRiesgo::constructorvacio();
                          $datos=$grupo->mostrarGrupo(); 
                          while( $fila = $datos->fetch_array()){ ?>
                            <option value=<?php echo $fila['codigo']?>><?php echo $fila['descripcion'] ?></option>
                          <?php } ?>
                          </select>
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" >Fecha y Hora de Ingreso
                        </label>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                          <input id="fechaIngreso" class="form-control col-md-7 col-xs-12" name="fechaIngreso"  required="required" type="date">
                        </div>
                        <div  class="col-md-2 col-sm-6 col-xs-12">
                        <input type="time" name="horaIngreso" step="" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div> 
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" >Responsable <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="serie" class="form-control col-md-7 col-xs-12" name="responsable" placeholder="" required="required" type="text">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" >G <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="serie" class="form-control col-md-7 col-xs-12" name="g" placeholder="" required="required" type="text">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" >P1 <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="serie" class="form-control col-md-7 col-xs-12" name="p1" placeholder="" required="required" type="text">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" >P2 <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="serie" class="form-control col-md-7 col-xs-12" name="p2" placeholder="" required="required" type="text">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" >P3 <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="serie" class="form-control col-md-7 col-xs-12" name="p3" placeholder="" required="required" type="text">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" >P4 <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="serie" class="form-control col-md-7 col-xs-12" name="p4" placeholder="" required="required" type="text">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" >EG (s) <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="serie" class="form-control col-md-7 col-xs-12" name="eg" placeholder="" required="required" type="text">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" >Nº CPN <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="serie" class="form-control col-md-7 col-xs-12" name="cpn" placeholder="" required="required" type="text">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Periodo de Complicación</label>
                        <div class="col-md-6 col-sm-9 col-xs-12">
                          <select class="form-control" name="periodo">
                            <option value="EMBARAZO">EMBARAZO</option>
                            <option value="PARTO">PARTO</option>
                            <option value="PUERPERIO">PUERPERIO</option>
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Estado Actual</label>
                        <div class="col-md-6 col-sm-9 col-xs-12">
                          <select class="form-control" name="estado">
                            <option value="ESTABLE">ESTABLE</option>
                            <option value="RESERVADO">RESERVADO</option>
                            <option value="FALLECIDA">FALLECIDA</option>
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                       <label class="control-label col-md-3 col-sm-3 col-xs-12">Comentarios</label>
                       <div class="col-md-6 col-sm-6 col-xs-12">
                        <textarea class="resizable_textarea form-control" name="comentarios"></textarea>
                       </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" >Fecha y Hora de Egreso
                        </label>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                          <input id="fechaEgreso" class="form-control col-md-7 col-xs-12" name="fechaEgreso"  type="date">
                        </div>
                        <div  class="col-md-2 col-sm-6 col-xs-12">
                        <input type="time" name="horaEgreso" step="" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div> 
                    </div>
                    <!-- DATO USUARIO-->
                    <input type="text" style="display:none;" name="usuario" value="<?php echo $_SESSION['usuario']['dni']?>">
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
                          <button type="submit" class="btn btn-primary">Cancel</button>
                          <button id="send" type="submit" class="btn btn-success">Registrar</button>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            Red de Salud - 2018
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="vendors/nprogress/nprogress.js"></script>
    <!-- validator -->
    <script src="vendors/validator/validator.js"></script>

    <!-- bootstrap-datetimepicker -->    
    <script src="vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="build/js/custom.min.js"></script>
        <!-- Initialize datetimepicker -->
  </body>
</html>