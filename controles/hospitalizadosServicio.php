<script src="js/idioma.js"></script>
<?php
include_once ('../entidades/control.php');
include_once ('../entidades/conexion.php');

$cn = new Conexion();
$control = Control::constructorvacio();
$resultado = $control->mostrarHospitalizados();
$salida = "";


if($resultado ->num_rows > 0){
    $salida.= '<table id="myTable" class="display nowrap">
                <thead>
                    <tr>
                        <th>CODIGO</th>
                        <th>Paciente</th>
                          <th>Ingreso</th>
                          <th>Hora de Ingreso</th>
                          <th>Serie</th>
                          <th>Red que Reporta</th>
                          <th>Renaes origen</th>
                          <th>ESS Origen</th>
                          <th>Distrito Origen</th>
                          <th>Provincia Origen</th>
                          <th>Renaes Destino</th>
                          <th>ESS Destino</th>
                          <th>Distrito Destino</th>
                          <th>Provincia Destino</th>
                          <th>Cie-01</th>
                          <th>Cie-Descriptivo</th>
                          <th>Cie-02</th>
                          <th>Cie-Descriptivo</th>
                          <th>Cie-03</th>
                          <th>Cie-Descriptivo</th>
                          <th>Cie-04</th>
                          <th>Cie-Descriptivo</th>
                          <th>Grupo de Riesgo</th>
                          <th>Inicio de Complicacion</th>
                          <th>Hora de Inicio</th>
                          <th>Fecha Ingreso</th>
                          <th>Responsable de la Referencia</th>
                          <th>Dni de Paciente</th>
                          <th>Apellido Paterno</th>
                          <th>Apellido Materno</th>
                          <th>Nombres</th>
                          <th>Direccion Actual</th>
                          <th>fechanacimiento</th>
                          <th>G</th>
                          <th>P1</th>
                          <th>P2</th>
                          <th>P3</th>
                          <th>P4</th>
                          <th>EG(S)</th>
                          <th>Nº CPN</th>
                          <th>Periodo de Complicacion</th>
                          <th>Estado Actual</th>
                          <th>Comentarios</th>
                          <th>Fecha de Egreso</th>
                          <th>Hora de Egreso</th>
                          <th>Acción</th>
                    </tr>
                </thead>
                <tbody>';
    while($fila = $resultado->fetch_array() ){
        $salida.="<tr>
                    <td>".$fila['codigoControl']."</td> 
                    <td>".$fila['ppaciente']."</td>
                    <td>".$fila['fechaIngreso']."</td>
                    <td>".$fila['horaDeIngreso']."</td>
                    <td>".$fila['codigoSerie']."</td>
                    <td>".$fila['codigoRed']."</td>
                    <td>".$fila['codigoOrigen']."</td>
                    <td>".$fila['essOrigen']."</td>
                    <td>".$fila['distritoOrigen']."</td>
                    <td>".$fila['provinciaOrigen']."</td>
                    <td>".$fila['codigoDestino']."</td>
                    <td>".$fila['essDestino']."</td>
                    <td>".$fila['distritoDestino']."</td>
                    <td>".$fila['provinciaDestino']."</td>
                    <td>".$fila['codigoCi1']."</td>
                    <td>".$fila['descripcionC1']."</td>
                    <td>".$fila['codigoCi2']."</td>
                    <td>".$fila['descripcionC2']."</td>
                    <td>".$fila['codigoCi3']."</td>
                    <td>".$fila['descripcionC3']."</td>
                    <td>".$fila['codigoCi4']."</td>
                    <td>".$fila['descripcionC4']."</td>
                    <td>".$fila['grupoRiesgo']."</td>
                    <td>".$fila['fechaInicio']."</td>
                    <td>".$fila['horaInicio']."</td>
                    <td>".$fila['fechaIngreso']."</td>
                    <td>".$fila['responsable']."</td>
                    <td>".$fila['dni']."</td>
                    <td>".$fila['apellidoPaterno']."</td>
                    <td>".$fila['apellidoMaterno']."</td>
                    <td>".$fila['nombres']."</td>
                    <td>".$fila['direccion']."</td>
                    <td>".$fila['fechanacimiento']."</td>
                    <td>".$fila['g']."</td>
                    <td>".$fila['p1']."</td>
                    <td>".$fila['p2']."</td>
                    <td>".$fila['p3']."</td>
                    <td>".$fila['p4']."</td>
                    <td>".$fila['eg']."</td>
                    <td>".$fila['cpn']."</td>
                    <td>".$fila['periodo']."</td>
                    <td>".$fila['estado']."</td>
                    <td>".$fila['comentarios']."</td>
                    <td>".$fila['fechaEgreso']."</td>
                    <td>".$fila['horaDeEgreso']."</td>
                    <td><button class='btn btn-success' onclick='actualizarControl(".$fila['codigoControl'].")'>Actualizar</button></td>
                 </tr>";
    }
    $salida.="</tbody></table>";
}else{
    $salida.="no hay datos";
}
"<script src='../js/reportesModificar.js.'></script>";
echo $salida;

