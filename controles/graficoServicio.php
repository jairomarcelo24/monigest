<?php
include_once ('../entidades/contador.php');

$contador = Contador::constructorvacio();
$resultado = $contador->contarControles();

$contadorH = Contador::constructorvacio();
$contadorHH = $contadorH->contarHospitalizados();

$contadorE = Contador::constructorvacio();
$contadorEE = $contadorE->contarEgresos();



$salida = "";

if(isset($_POST['dia_inicio']) && isset($_POST['dia_fin'])){
    $dia_inicio = $_POST['dia_inicio'];
    $dia_fin = $_POST['dia_fin']; 
    $resultado = $contador->filtrarFechaControles($dia_inicio, $dia_fin);
    $contadorHH = $contadorH->filtrarFechaHospitalizados($dia_inicio,$dia_fin);
    $contadorEE = $contadorE->filtrarFechaEgresos($dia_inicio,$dia_fin);
}

if ($fila = mysqli_fetch_array($resultado)){
    $salida.= '<div class="row tile_count">
    <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
    <span class="count_top"><i class="fa fa-clock-o"></i>Controles</span>
    <div class="count">'.$fila['cantidadreferencias'].'</div>
    <span class="count_bottom"><i class="green">Total de Controles</i></span>
    </div>'; 
}

if($filaH = mysqli_fetch_array($contadorHH)){
    $salida.= '
    <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
    <span class="count_top"><i class="fa fa-clock-o"></i>Hospitalizados</span>
    <div class="count">'.$filaH['cantidadreferencias'].'</div>
    <span class="count_bottom"><i class="green">Total de Hospitalizados</i></span>
    </div>'; 
}

if($filaE = mysqli_fetch_array($contadorEE)){
    $salida.= '
    <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
    <span class="count_top"><i class="fa fa-clock-o"></i>Egresos</span>
    <div class="count">'.$filaE['cantidadreferencias'].'</div>
    <span class="count_bottom"><i class="green">Total de Egresos</i></span>
    </div>'; 
}


echo $salida;
