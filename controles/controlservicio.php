<?php
    include ('../entidades/paciente.php');
    include ('../entidades/control.php');

        // DATOS PACIENTE
        $dni = $_POST["dni"];
      
        //////////////////////////////////////////////////

        // DATOS DEL CONTROL
        // Fecha Inicio de complicacion
        $fechaInicioComplicacion = $_POST["fechaInicio"];
        $horaInicio = $_POST["horaInicio"];
        // -----
        $serie=$_POST["serie"];
        $red=$_POST["cboRed"];
        $renaesOrigen=$_POST["cborenaesOrigen"];
        $renaesDestino=$_POST["cborenaesDestino"];
        $cie01=$_POST["cbocie01"];
        $cie02=$_POST["cbocie02"];
        $cie03=$_POST["cbocie03"];
        $cie04=$_POST["cbocie04"];
        $grupoRiesgo=$_POST["cbogrupoRiesgo"];
        // FECHA INGRESO
        $fechaIngreso=$_POST["fechaIngreso"];   
        $horaIngreso = $_POST["horaIngreso"];
        //-----
        $responsable=$_POST["responsable"];
        $g=$_POST["g"];
        $p1=$_POST["p1"];
        $p2=$_POST["p2"];
        $p3=$_POST["p3"];
        $p4=$_POST["p4"];
        $eg=$_POST["eg"];
        $cpn=$_POST["cpn"];
        $periodo=$_POST["periodo"];
        $estado=$_POST["estado"];
        $comentarios=$_POST["comentarios"];
        // FECHA EGRESO
        $fechaEgreso=$_POST["fechaEgreso"];
        $horaEgreso=$_POST["horaEgreso"];
        //---
        // OBTENEMOS EL CODIGO DEL ULTIMO PACIENTE INGRESADO
       // $ultimoPaciente=$paciente->ultimoPaciente()

       //VALOR DE LA ACCCION A REALZIAR
       $funcion = $_POST["funcion"];

       $codigoAmodificar = $_POST["codigoAmodificar"];

        // Creamos Paciente
        $paciente = Paciente::constructorvacio();
        //Creamos Control y preparamos para la inserción
        $usuario=$_POST['usuario'];
        $control=new Control($cie01,$cie02,$cie03,$cie04,$dni,$comentarios,$cpn,$eg,$estado,$fechaInicioComplicacion,$horaInicio,$fechaIngreso, $horaIngreso,$fechaEgreso,$horaEgreso,$g,$grupoRiesgo,$p1,$p2,$p3,$p4,$periodo,$red,$renaesOrigen,$renaesDestino,$responsable,$serie,$usuario);

        
        //VALIDAMOS QUE ACCCION REALIZAREMOS
        if (strcmp($funcion,"modificar" === 0)) {
            
            $control-> actualizarControl($codigoAmodificar);
            header("Location: ../reportes.php");
           
        } else {

            // verificamos si el paciente esta registrado
            $resultado=$paciente->buscarPaciente($dni);
            //  verificamos si el paciente ya tiene un control
         //   $resultadoControl=$control->buscarControlPaciente($dni);

            if ($resultado->num_rows == 0) {
                echo "<script languaje='javascript'> alert('ERROR :: El Paciente no esta registrado')</script>";
                echo "<script languaje='javascript'> window.location='../registroPacientes.php'</script>";
            /*  }elseif ($resultadoControl->num_rows == 1) {
                echo "<script languaje='javascript'> alert('ERROR :: El Paciente ya tiene un control')</script>";
                echo "<script languaje='javascript'> window.location='../reportes.php'</script>";*/
            }else{
                $control->insertarControl();
                header("Location:../registroGestantes.php");
            }
            
        }
        



?>