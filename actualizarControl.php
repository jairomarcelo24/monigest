<?php
    include "entidades/control.php";
    session_start();
    if(!isset($_SESSION['usuario'])){
        header("Location: index.php");
    }

    if(isset($_GET["parametro"])){
        $codigoControl=$_GET["parametro"];
        $control=Control::constructorvacio();
        $resultado=$control->buscarControl($codigoControl);
        while( $data = $resultado->fetch_array()){
    
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Gestantes Utes </title>

    <!-- Bootstrap -->
    <link href="vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="vendors/nprogress/nprogress.css" rel="stylesheet">
    
    <!-- Custom Theme Style -->
    <link href="build/css/custom.min.css" rel="stylesheet">
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="home.php" class="site_title"><i class="fa fa-paw"></i> <span>MONIGEST</span></a>
            </div>

            <div class="clearfix"></div>
           <!-- INCLUIMOS EL MENU -->
           <?php include "menu.php"; ?>
           <!-- FIN DE MENU -->

        <!-- page content -->
        <div class="right_col" role="main">
            <div class="page-title">
              <div class="title_left">  
                <h3>Registro de Control</h3>
              </div>
            </div>
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Control<small></small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <form action="controles/controlservicio.php" method="POST" class="form-horizontal form-label-left" novalidate>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" >Numero de Referencia <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="serie" class="form-control col-md-7 col-xs-12" name="serie" value="<?php echo $data["codigoSerie"] ?>" required="required" type="text">
                        </div>
                      </div>
                      
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" >Dni del Paciente<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="dni" class="form-control col-md-7 col-xs-12"  name="" value="<?php echo $data["dniPaciente"] ?>" required="required" type="text" disabled >
                          <input type="hidden" value="<?php echo $data["dniPaciente"] ?>" name="dni">
                        </div>
                      </div>

                        <!-- DATOS PROPIOS DEL CONTROL -->
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" >Fecha y Hora de Inicio de Complicación
                        </label>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                          <input id="fechaInicio" class="form-control col-md-7 col-xs-12" name="fechaInicio"  value="<?php echo $data["fechaInicio"]?>" required="required" type="date">
                        </div>
                        <div  class="col-md-2 col-sm-6 col-xs-12">
                        <input type="time" name="horaInicio" value = "<?php echo $data["horaInicio"]?>" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>


                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Red que reporta</label>
                        <div class="col-md-6 col-sm-9 col-xs-12">
                          <select class="form-control" name="cboRed">
                            <option value="<?php echo $data["codigoRed"]?>"><?php echo $data["codigoRed"]?></option>
                            <option value="NO TIENE ASIGNADO NINGUNA RED">NO TIENE ASIGNADO NINGUNA RED</option>
                            <option value="RED ASCOPE">RED ASCOPE</option>
                            <option value="RED BOLIVAR">RED BOLIVAR</option>
                            <option value="RED CHEPEN">RED CHEPEN</option>
                            <option value="RED GRAN CHIMU">RED GRAN CHIMU</option>
                            <option value="RED JULCAN">RED JULCAN</option>
                            <option value="RED OTUZCO">RED OTUZCO</option>
                            <option value="RED PACASMAYO">RED PACASMAYO</option>
                            <option value="RED PATAZ">RED PATAZ</option>
                            <option value="RED SANCHEZ CARRION">RED SANCHEZ CARRION</option>
                            <option value="RED SANTIAGO DE CHUCO">RED SANTIAGO DE CHUCO</option>
                            <option value="RED TRUJILLO">RED TRUJILLO</option>
                            <option value="RED VIRU">RED VIRU</option>
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Renaes Origen</label>
                        <div class="col-md-6 col-sm-9 col-xs-12">
                          <select class="form-control" name="cborenaesOrigen">
                          <option value="<?php echo $data['codigoOrigen']?>"><?php echo $data['codigoOrigen']." - ".$data['essOrigen']." - ".$data['distritoOrigen']." - ".$data['provinciaOrigen'] ?></option>
                          <?php include_once "entidades/renaes.php" ;
                          $renaes= Renaes::constructorvacio();
                          $datos=$renaes->mostrarRenaes(); 
                          while( $fila = $datos->fetch_array()){ ?>
                            <option value=<?php echo $fila['codigo']?>><?php echo $fila['codigo']." - ".$fila['ess']." - ".$fila['distrito']." - ".$fila['provincia']?></option>
                          <?php } ?>
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Renaes Destino</label>
                        <div class="col-md-6 col-sm-9 col-xs-12">
                          <select class="form-control" name="cborenaesDestino">
                          <option value="<?php echo $data['codigoDestino']?>"><?php echo $data['codigoDestino']." - ".$data['essDestino']." - ".$data['distritoDestino']." - ".$data['provinciaDestino'] ?></option>
                          <?php include_once "entidades/renaes.php" ;
                          $renaes= Renaes::constructorvacio();
                          $datos=$renaes->mostrarRenaes(); 
                          while( $fila = $datos->fetch_array()){ ?>
                            <option value=<?php echo $fila['codigo']?>><?php echo $fila['codigo']." - ".$fila['ess']." - ".$fila['distrito']." - ".$fila['provincia']?></option>
                          <?php } ?>
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">CIE-01</label>
                        <div class="col-md-6 col-sm-9 col-xs-12">
                          <select class="form-control" name="cbocie01"> 
                          <option value="<?php echo $data['codigoCi1']?>"><?php echo $data['descripcionC1'] ?></option>
                          <?php include_once "entidades/cie.php" ;
                          $cie= Cie::constructorvacio();
                          $datos=$cie->mostrarCie(); 
                          while( $fila = $datos->fetch_array()){ ?>
                            <option value=<?php echo $fila['codigo']?>><?php echo  $fila['codigo']." - ".$fila['descripcion'] ?></option>
                          <?php } ?>
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">CIE-02</label>
                        <div class="col-md-6 col-sm-9 col-xs-12">
                          <select class="form-control" name="cbocie02">
                          <option value="<?php echo $data['codigoCi2']?>"><?php echo $data['descripcionC2'] ?></option>
                          <?php include_once "entidades/cie.php" ;
                          $cie= Cie::constructorvacio();
                          $datos=$cie->mostrarCie(); 
                          while( $fila = $datos->fetch_array()){ ?>
                            <option value=<?php echo $fila['codigo']?>><?php echo  $fila['codigo']." - ".$fila['descripcion'] ?></option>
                          <?php } ?>
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">CIE-03</label>
                        <div class="col-md-6 col-sm-9 col-xs-12">
                          <select class="form-control" name="cbocie03">
                          <option value="<?php echo $data['codigoCi3']?>"><?php echo $data['descripcionC3'] ?></option>
                          <?php include_once "entidades/cie.php" ;
                          $cie= Cie::constructorvacio();
                          $datos=$cie->mostrarCie(); 
                          while( $fila = $datos->fetch_array()){ ?>
                            <option value=<?php echo $fila['codigo']?>><?php echo  $fila['codigo']." - ".$fila['descripcion'] ?></option>
                          <?php } ?>
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">CIE-04</label>
                        <div class="col-md-6 col-sm-9 col-xs-12">
                          <select class="form-control" name="cbocie04">
                          <option value="<?php echo $data['codigoCi4']?>"><?php echo $data['descripcionC4'] ?></option>
                          <?php include_once "entidades/cie.php" ;
                          $cie= Cie::constructorvacio();
                          $datos=$cie->mostrarCie(); 
                          while( $fila = $datos->fetch_array()){ ?>
                            <option value=<?php echo $fila['codigo']?>><?php echo  $fila['codigo']." - ".$fila['descripcion'] ?></option>
                          <?php } ?>
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Grupo de Riesgo</label>
                        <div class="col-md-6 col-sm-9 col-xs-12">
                          <select class="form-control" name="cbogrupoRiesgo">
                          <option value="<?php echo $data['codigogrupo']?>"><?php echo $data['grupoRiesgo'] ?></option>
                          <?php include_once "entidades/gruporiesgo.php" ;
                          $grupo= GrupoRiesgo::constructorvacio();
                          $datos=$grupo->mostrarGrupo(); 
                          while( $fila = $datos->fetch_array()){ ?>
                            <option value=<?php echo $fila['codigo']?>><?php echo $fila['descripcion'] ?></option>
                          <?php } ?>
                          </select>
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" >Fecha y Hora de Ingreso
                        </label>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                          <input id="fechaIngreso" class="form-control col-md-7 col-xs-12" name="fechaIngreso"  value="<?php echo $data["fechaIngreso"]?>" required="required" type="date">
                        </div>
                        <div  class="col-md-2 col-sm-6 col-xs-12">
                        <input type="time" name="horaIngreso" step="" class="form-control col-md-7 col-xs-12" value="<?php echo $data["horaDeIngreso"]?>">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" >Responsable <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="serie" class="form-control col-md-7 col-xs-12" name="responsable" value="<?php echo $data["responsable"]?>" required="required" type="text">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" >G <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="serie" class="form-control col-md-7 col-xs-12" name="g" value="<?php echo $data["g"]?>" required="required" type="text">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" >P1 <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="serie" class="form-control col-md-7 col-xs-12" name="p1" value="<?php echo $data["p1"]?>" required="required" type="text">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" >P2 <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="serie" class="form-control col-md-7 col-xs-12" name="p2" value="<?php echo $data["p2"]?>" required="required" type="text">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" >P3 <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="serie" class="form-control col-md-7 col-xs-12" name="p3" value="<?php echo $data["p3"]?>" required="required" type="text">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" >P4 <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="serie" class="form-control col-md-7 col-xs-12" name="p4" value="<?php echo $data["p4"]?>" required="required" type="text">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" >EG (s) <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="serie" class="form-control col-md-7 col-xs-12" name="eg" value="<?php echo $data["eg"]?>" required="required" type="text">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" >Nº CPN <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="serie" class="form-control col-md-7 col-xs-12" name="cpn" value="<?php echo $data["cpn"]?>" required="required" type="text">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Periodo de Complicación</label>
                        <div class="col-md-6 col-sm-9 col-xs-12">
                          <select class="form-control" name="periodo">
                            <option value="<?php echo $data["periodo"]?>"><?php echo $data["periodo"]?></option>
                            <option value="EMBARAZO">EMBARAZO</option>
                            <option value="PARTO">PARTO</option>
                            <option value="PUERPERIO">PUERPERIO</option>
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Estado Actual</label>
                        <div class="col-md-6 col-sm-9 col-xs-12">
                          <select class="form-control" name="estado">
                            <option value="<?php echo $data["estado"]?>"><?php echo $data["estado"]?></option>
                            <option value="ESTABLE">ESTABLE</option>
                            <option value="RESERVADO">RESERVADO</option>
                            <option value="FALLECIDA">FALLECIDA</option>
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                       <label class="control-label col-md-3 col-sm-3 col-xs-12">Comentarios</label>
                       <div class="col-md-6 col-sm-6 col-xs-12">
                        <textarea class="resizable_textarea form-control" name="comentarios"><?php echo $data['comentarios']?></textarea>
                       </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" >Fecha y Hora de Egreso
                        </label>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                          <input id="fechaEgreso" class="form-control col-md-7 col-xs-12" name="fechaEgreso" value="<?php  echo $data["fechaEgreso"]?>" type="date">
                        </div>
                        <div  class="col-md-2 col-sm-6 col-xs-12">
                        <input type="time" name="horaEgreso" step="" class="form-control col-md-7 col-xs-12" value="<?php echo $data["horaDeEgreso"]?>">
                        </div>
                      </div> 
                    </div>
                          <?php } }?>
                    <!-- DATO USUARIO-->
                    <input type="text" style="display:none;" name="usuario" value="<?php echo $_SESSION['usuario']['dni']?>">
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
                          <a href="reportes.php" class="btn btn-primary">Cancelar</a>
                          <button id="send" type="submit" class="btn btn-success">Actualizar</button>
                        </div>
                      </div>
                      <input type="hidden" name="funcion" value = "modificar" />
                      <input type="hidden" name="codigoAmodificar" value="<?php echo $codigoControl ?>">
                    </form>
                  </div>
                </div>
              </div>
          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            Red de Salud - 2018
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="vendors/nprogress/nprogress.js"></script>
    <!-- validator -->
    <script src="vendors/validator/validator.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="build/js/custom.min.js"></script>
	
  </body>
</html>