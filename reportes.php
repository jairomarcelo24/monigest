<?php
session_start();
if(!isset($_SESSION['usuario'])){
   header("Location: index.php");
}

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Monigest</title>

    <!--- CSS DE LA TABLA -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.18/b-1.5.4/b-colvis-1.5.4/b-flash-1.5.4/b-html5-1.5.4/b-print-1.5.4/r-2.2.2/datatables.min.css"/>

    <!-- Bootstrap -->
    <link href="vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="vendors/nprogress/nprogress.css" rel="stylesheet">
    
    

    <!-- Custom Theme Style -->
    <link href="build/css/custom.min.css" rel="stylesheet">
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="home.php" class="site_title"><i class="fa fa-paw"></i> <span>MONIGEST</span></a>
            </div>

            <div class="clearfix"></div>
              <!-- INCLUIMOS EL MENU -->
              <?php include "menu.php"; ?> 
              <!-- FIN DE MENU -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>REPORTE DE SEGUIMIENTOS</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix">
                    </div>
                  </div>
                  
                  <div class="x_content">
                  <br>
                  <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" >SELECCIONE LAS FECHAS:</label>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                          <input class="form-control col-md-7 col-xs-12" id="dia_inicio" name="dia_inicio" type="date">
                        </div> 
                        <div class="col-md-3 col-sm-6 col-xs-12">
                          <input class="form-control col-md-7 col-xs-12" id="dia_fin" name ="dia_fin" type="date">
                        </div> 
                        <button class="btn btn-success" type="submit" id="filtrar">FILTRAR</button>
                  </div>
                  <br>

                  <div id="datos" >
                  
                  
                  </div>
                   
                  </div>
                
                </div>
              </div>
              </div>
             
            </div>
          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
          Red de Salud - 2018
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    
    <!-- jQuery -->
    <script src="vendors/jquery/dist/jquery.min.js"></script>

    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.18/b-1.5.4/b-colvis-1.5.4/b-flash-1.5.4/b-html5-1.5.4/b-print-1.5.4/r-2.2.2/datatables.min.js"></script>
    <!-- Bootstrap -->
    <script src="vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="vendors/nprogress/nprogress.js"></script>
  

    
    <script src="build/js/custom.min.js"></script>             
    <script src="js/reportesModificar.js"></script>
    <script src="js/filtrarReporte.js"></script>                         
  </body>
</html>